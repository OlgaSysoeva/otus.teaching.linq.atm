﻿using System;
using System.Collections.Generic;
using System.Linq;

using Otus.Teaching.Linq.ATM.Core.Entities;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;

using SysCons = System.Console;

namespace Otus.Teaching.Linq.ATM.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            SysCons.WriteLine("Старт приложения-банкомата...");

            var atmManager = CreateATMManager();

            //TODO: Далее выводим результаты разработанных LINQ запросов
            AccountInform(atmManager);
            
            SysCons.WriteLine("Завершение работы приложения-банкомата...");
        }

        static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();
                
            return new ATMManager(accounts, users, history);
        }

        static void AccountInform(ATMManager atmManager)
        {
            var user = GetUser(atmManager);

            if (user == null)
            {
                SysCons.WriteLine("Пользователь не найден.");
                return;
            }

            WriteLineUser(user);

            var accounts = WriteAccounts(atmManager, user);

            WriteHystores(atmManager, accounts);

            WriteInputOperation(atmManager);

            WriteUsersWithSum(atmManager);
        }

        private static User GetUser(ATMManager atmManager)
        {
            SysCons.WriteLine("Вывод информации по логину и паролю.");

            SysCons.WriteLine("Введите логин пользователя: ");
            string login = SysCons.ReadLine();
            SysCons.WriteLine("Введите пароль пользователя: ");
            string password = SysCons.ReadLine();

            var user = atmManager.GetUserInformation(login, password);

            return user;
        }

        private static void WriteLineUser(User user)
        {
            SysCons.WriteLine($"Номер: {user.Id} " +
                $"Пользователь: {user.SurName} {user.FirstName} " +
                $"{user.MiddleName} Телефон: {user.Phone} " +
                $"Данные паспорта: {user.PassportSeriesAndNumber} " +
                $"Дата регистрации: {user.RegistrationDate}");
        }

        private static void WriteLineAccount(Account account)
        {
            SysCons.WriteLine($"Номер: {account.Id} " +
                $"Дата открытия счета: {account.OpeningDate} " +
                $"Сумма на счете: {account.CashAll} ");
        }

        private static void WriteLineHystory(IEnumerable<OperationsHistory> history)
        {
            foreach (var itemHistory in history)
            {
                SysCons.WriteLine($"\tНомер: {itemHistory.Id} " +
                $"Дата операции: {itemHistory.OperationDate} " +
                $"Тип операции: {itemHistory.OperationType} " +
                $"Сумма: {itemHistory.CashSum}");
            }
        }

        private static IEnumerable<Account> WriteAccounts(ATMManager atmManager, User user)
        {
            SysCons.WriteLine("Счета текущего пользователя:");

            var accounts = atmManager.GetAccountsByUserId(user.Id);

            if (accounts.Count() == 0)
            {
                SysCons.WriteLine("У пользователя отсутствуют счета.");
                return accounts;
            }

            foreach (var item in accounts)
            {
                WriteLineAccount(item);
            }

            return accounts;
        }

        private static void WriteHystores(ATMManager atmManager, IEnumerable<Account> accounts)
        {
            SysCons.WriteLine("История счетов пользователя:");

            if (accounts.Count() == 0)
            {
                SysCons.WriteLine("История отсутствует.");
                return;
            }

            foreach (var itemAccount in accounts)
            {
                WriteLineAccount(itemAccount);

                var history = atmManager.GetHistoryByAccountId(itemAccount.Id);

                WriteLineHystory(history);
            }
        }

        private static void WriteInputOperation(ATMManager atmManager)
        {
            SysCons.WriteLine("Операции пополнения счета:");

            var accountAll = atmManager.GetAllAccount();
            var users = atmManager.GetUserNames();

            foreach (var itemAccount in accountAll)
            {

                if (!users.Any(x => x.Id == itemAccount.UserId))
                {
                    continue;
                }

                var owner = users
                    .Single(x => x.Id == itemAccount.UserId);

                SysCons.WriteLine($"Номер: {itemAccount.Id} " +
                    $"Владелец: {owner.SurName} {owner.FirstName} {owner.MiddleName} " +
                    $"Дата открытия счета: {itemAccount.OpeningDate} " +
                    $"Сумма на счете: {itemAccount.CashAll} ");

                var history = atmManager.GetHistoryInputCash(itemAccount.Id);

                WriteLineHystory(history);
            }
        }

        private static void WriteUsersWithSum(ATMManager atmManager)
        {
            SysCons.WriteLine("Информация о пользователях с суммой на счете больше N:");

            SysCons.WriteLine("Введите минимальную сумму: ");

            decimal sum;
            try
            {
                sum = Convert.ToDecimal(SysCons.ReadLine());
            }
            catch
            {
                SysCons.WriteLine("Некорректные данные.");
                return;
            }

            var userIds = atmManager.GetUserIdsByMinSumAccount(sum);

            var userList = atmManager.GetUsersByIds(userIds);

            if (userList.Count() == 0)
            {
                SysCons.WriteLine("Пользователи отсутствуют.");
                return;
            }

            foreach (var item in userList)
            {
                WriteLineUser(item);
            }
        }
    }
}