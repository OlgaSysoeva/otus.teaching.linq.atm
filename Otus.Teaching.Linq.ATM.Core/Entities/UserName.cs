﻿namespace Otus.Teaching.Linq.ATM.Core.Entities
{
    public class UserName
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string SurName { get; set; }

        public string MiddleName { get; set; }
    }
}
