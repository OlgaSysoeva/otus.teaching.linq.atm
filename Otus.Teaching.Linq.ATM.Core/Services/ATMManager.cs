﻿using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Entities;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }
        
        public IEnumerable<User> Users { get; private set; }
        
        public IEnumerable<OperationsHistory> History { get; private set; }
        
        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }

        //TODO: Добавить методы получения данных для банкомата

        /// <summary>
        /// Получение счетов пользователя.
        /// </summary>
        /// <param name="userID">Идентификатор пользователя.</param>
        /// <returns>Возвращает список счетов.</returns>
        public IEnumerable<Account> GetAccountsByUserId(int userId)
        {
            var accounts = Accounts
                .Where(x => x.UserId == userId);

            return accounts;
        }

        /// <summary>
        /// Получает информацию всех счетов.
        /// </summary>
        /// <returns>Возвращает список счетов.</returns>
        public IEnumerable<Account> GetAllAccount()
        {
            return Accounts;
        }

        /// <summary>
        /// Получение истории операций по номеру счета.
        /// </summary>
        /// <param name="accountId">Идентификатор счета.</param>
        /// <returns>Возвращает список истории операций.</returns>
        public IEnumerable<OperationsHistory> GetHistoryByAccountId(int accountId)
        {
            var history = History
                .Where(x => x.AccountId == accountId);

            return history;
        }

        /// <summary>
        /// Получает историю операций пополнения счета.
        /// </summary>
        /// <param name="accountId">Идентификатор счета.</param>
        /// <returns>Возвращает список истории операций.</returns>
        public IEnumerable<OperationsHistory> GetHistoryInputCash(int accountId)
        {
            var history = History
                .Where(x => x.AccountId == accountId
                    && x.OperationType == OperationType.InputCash);

            return history;
        }

        /// <summary>
        /// Получает идентификаторы пользователей у которых на счету сумма, больше указанной.
        /// </summary>
        /// <param name="sum">Минимальная сумма счета.</param>
        /// <returns>Возвращает список идентификаторов пользователей</returns>
        public IEnumerable<int> GetUserIdsByMinSumAccount(decimal sum)
        {
            var userIds = Accounts
                .Where(x => x.CashAll > sum)
                .Distinct()
                .Select(x => x.UserId);

            return userIds;
        }

        /// <summary>
        /// Поиск пользователя по логину и паролю.
        /// </summary>
        /// <param name="login">Логин пользователя.</param>
        /// <param name="password">Пароль пользователя.</param>
        /// <returns>Возвращает данные пользователя.</returns>
        public User GetUserInformation(string login, string password)
        {
            var user = Users
                .Where(x => x.Login == login
                    && x.Password == password)
                .FirstOrDefault();

            return user;
        }

        /// <summary>
        /// Получает ФИО всех пользователей.
        /// </summary>
        /// <returns>Возвращает словарь с идентификатором и ФИО.</returns>
        public IEnumerable<UserName> GetUserNames()
        {
            var users = Users
                .Select(x => new UserName
                {
                    Id = x.Id,
                    FirstName = x.FirstName,
                    SurName = x.SurName,
                    MiddleName = x.MiddleName
                })
                .ToList();
            return users;
        }

        /// <summary>
        /// Получает пользователей по набору идентификаторов.
        /// </summary>
        /// <param name="ids">Список идентификаторов.</param>
        /// <returns>Возвращает список пользователей.</returns>
        public  IEnumerable<User> GetUsersByIds(IEnumerable<int> ids)
        {
            var users = Users
               .Where(x => ids.Contains(x.Id));

            return users;
        }
    }
}